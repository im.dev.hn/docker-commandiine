# [docker] 설치
1. 도커 회원가입
2. [https://hub.docker.com/?overlay=onboarding](https://hub.docker.com/?overlay=onboarding) 들어가 도커 다운로드 한다.
3. 다운로드 후 가이드에 따라 그대로 진행한다(진행시 윈도우10의 powershell을 사용한다.)


# [docker] oralce 11g setup
0. docker login
*  docker 로그인
1. docker pull jaspeen/oracle-xe-11g 
*  docker에 jaspeen/oracle-xe-11g 다운로드
2. docker images
*  docker에 다운로드 받은 img 출력
3. docker run --name oracle11g -d -p 1521:1521 jaspeen/oracle-xe-11g
*  docker 실행oracle11g 이름으로 -d : demon 실행 -p 1521 포트 열어서,
4. docker exec -it oracle11g sqlplus
*  docker에서 sqlplus명령으로 oracle11g 이름으로실행중인 컨테이너 참여

# [docker] oracle11g docker hub에 Push하기
1. docker commit $[container-name] $[repository$[:tag]]
*  도커 컨테이너이름 저장소(:태그)_____ex)docker commit oracle11g oracle11g
2. docker images 로 이미지 확인
3. docker tag $[저장하고싶은 컨테이너 이름] [docker_id]/$[저장될이미지이름$[:태그]]
*. docker tag oracle11g $DOCKER_ID_USER/oracle11g
4. docker push $DOCKER_ID_USER/oracle11g